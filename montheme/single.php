
  <?php 
get_header();


$image = get_field('image');
$picture = $image['sizes']['medium'];    

?>

<div class = "container-fluid">
    <h1 class="text-center m-5"><?php the_title(); ?></h1>


</div>


<div class="container g-0">
  <div class="row">
    <div class="col-8 section p-3">
    <div>Taille : <?php the_field( 'taille' ); ?> toises</div>
    <?php the_content(); ?>
    </div>


    <div class="col-4 section p-3">

    <img src=" <?php echo $picture;?>" class="img-fluid" alt = "enluminure de la bête">
    <div>Il mange </div>
  <div>
  <?php 
                $locations = get_field('regime');

                ?>
                <?php if( $locations ): ?>
                    <ul>
                    <?php foreach( $locations as $location ): ?>
                        <li>
                            <a href="<?php echo get_permalink( $location->ID ); ?>">
                                <?php echo get_the_title( $location->ID ); ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
  </div>

    </div>
  </div>
</div>


<div >



  
</div>


    <?php 
get_footer();
?>
  