


  <?php 
get_header();

?>

<div class = "container-fluid">
    <h1 class="text-center m-5">Les creatures de notre royaume, vive le roi !</h1>
</div>



<?php 

the_a_z_listing( $query );

if (have_posts()) : ?>
<div class="row">
    <?php while ( $a_z_query->have_posts() ) : $a_z_query->the_post() ; 
    
    ?>
    <div class="col-sm-4">
        <div class="card">
            <?php the_post_thumbnail('medium', ['class' => 'card-img-top', 'alt' => '', style => 'height:auto;']) ?>
            <div class="card-body">
                <h5 class="card-title"> <?php $a_z_query->the_title();?></h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <?php the_category();?>
                </h6>
                <p class="card-text">
                    <?php the_excerpt()?>
                </p>
                <a href="<?php the_permalink();?>" class="btn btn-primary card-link">voir plus </a>
            </div>
        </div>
    </div>
<?php endwhile ?>
</div>
<?php else : ?>
    <h1>pas d'articles</h1>
    <?php endif; ?>



    <?php 
get_footer();
?>
  